# Copyright 2017-2021 Utility Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy of
# the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#
# File name: utk.exception/CMakeLists.txt
#
# Description: utk.exception library dependencies.


#####################
# Find dependencies #
#####################
function (utk_exception_find_dependencies)
  if (WIN32)
    # According to documantation:
    #
    # On Visual Studio and Borland compilers Boost headers request automatic
    # linking to corresponding libraries. This requires matching libraries to be
    # linked explicitly or available in the link library search path. In this case
    # setting Boost_USE_STATIC_LIBS to OFF may not achieve dynamic linking. Boost
    # automatic linking typically requests static libraries with a few exceptions
    # (such as Boost.Python).
    #
    # That's why for static libraries it is required to explicitly enable static
    # libraries usage.
    set (Boost_USE_STATIC_LIBS ON)
  endif (WIN32)

  find_package (Boost 1.38 REQUIRED
    COMPONENTS
    exception
    )
endfunction (utk_exception_find_dependencies)


####################
# Use dependencies #
####################
function (utk_exception_use_dependencies)
  set (_options
    )
  set (_multi_value_args
    TARGET
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})

  if (NOT i_TARGET)
    message (SEND_ERROR "Provide IMPORTED_TARGET or INTERFACE_TARGET argument")
  endif ()

  foreach (_target IN LISTS i_TARGET)
    target_link_libraries (
      ${_target}
      PUBLIC
      Boost::boost
      Boost::exception
      )
  endforeach (_target IN LISTS _interface_targets)
endfunction (utk_exception_use_dependencies)
