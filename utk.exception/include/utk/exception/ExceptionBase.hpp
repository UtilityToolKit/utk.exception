// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name:
// utk.exception/utk.exception/include/utk/exception/ExceptionBase.hpp
//
// Description: The base class for the exception classes provided by the
//              library.


#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_EXCEPTIONBASE_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_EXCEPTIONBASE_HPP


#include <exception>
#include <string>

#include <boost/exception/error_info.hpp>
#include <boost/exception/exception.hpp>
#include <boost/exception/info_tuple.hpp>

#include "utk/exception/namespace.hpp"
#include "utk/exception/utk_exception_export.h"


UTK_EXCEPTION_NAMESPACE_OPEN


/**
   @brief The base class for the library exception classes

   @details The class inherits both boost::exception and std::exception. This
   allows for use of flexible mechanisms of exception information propagation
   provided by boost::exception while making it possible to catch an exception
   in code that uses std::exception based exception classes.
*/
struct UTK_EXCEPTION_EXPORT ExceptionBase : virtual boost::exception,
                                            virtual std::exception {
	typedef boost::error_info< struct Description, std::string > Description;

	const char* what () const throw ();
};


UTK_EXCEPTION_NAMESPACE_CLOSE


#endif /* UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_EXCEPTIONBASE_HPP */
