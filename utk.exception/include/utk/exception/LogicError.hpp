// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name:
// utk.exception/utk.exception/include/utk/exception/LogicError.hpp
//
// Description: The base class for logix error exception classes.


#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_LOGICERROR_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_LOGICERROR_HPP


#include "utk/exception/namespace.hpp"
#include "utk/exception/utk_exception_export.h"

#include "utk/exception/ExceptionBase.hpp"


UTK_EXCEPTION_NAMESPACE_OPEN


/**
   @brief The base class for logic error handling exception classes

   @details A logic error is an error in program logic and results from some
   kind of error in source code. Catching logic error during application
   execution should result in a bug report and a bug fix.
*/
struct LogicError : public ExceptionBase {};


UTK_EXCEPTION_NAMESPACE_CLOSE


#endif /* UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_LOGICERROR_HPP */
