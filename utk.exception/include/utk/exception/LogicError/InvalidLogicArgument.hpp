// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name:
// utk.exception/utk.exception/include/utk/exception/RuntimeError/InvalidLogicArgument.hpp
//
// Description: The base class for the runtime exception classes.


#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_LOGICERROR_INVALIDLOGICARGUMENT_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_LOGICERROR_INVALIDLOGICARGUMENT_HPP


#include "utk/exception/namespace.hpp"
#include "utk/exception/utk_exception_export.h"

#include "utk/exception/LogicError.hpp"


UTK_EXCEPTION_NAMESPACE_OPEN


/**
   @brief The exception class for reporting wrong function usage

   @details Exceptions of this type should be thrown whenever a function
   argument has an invalid value that most likely is a result of an error in
   function usage. This kind of error may be fixed by reading the source code
   and fixing the value of the problem argument.
*/
struct InvalidLogicArgument : public LogicError {
	typedef boost::error_info< struct ArgumentName, std::string > ArgumentName;
	typedef boost::tuple< ArgumentName, Description > ArgumentInfo;
};


UTK_EXCEPTION_NAMESPACE_CLOSE


#endif // UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_LOGICERROR_INVALIDLOGICARGUMENT_HPP
