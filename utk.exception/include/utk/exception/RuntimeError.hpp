// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name:
// utk.exception/utk.exception/include/utk/exception/RuntimeError.hpp
//
// Description: The base class for the runtime exception classes.


#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_HPP


#include "utk/exception/namespace.hpp"
#include "utk/exception/utk_exception_export.h"

#include "utk/exception/ExceptionBase.hpp"


UTK_EXCEPTION_NAMESPACE_OPEN


/**
   @brief The base class for runtime error handling exception classes

   @details A runtime error is any error that may occur during the application
   execution: invalid input from the user, missing file, network connection
   failure. Runtime errors do not usually mean there is a bug in the software.
*/
struct RuntimeError : public ExceptionBase {};


UTK_EXCEPTION_NAMESPACE_CLOSE


#endif /* UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_HPP */
