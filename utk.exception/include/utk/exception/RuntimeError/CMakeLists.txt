# Copyright 2017-2021 Utility Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy of
# the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#
#
# File name: utk.exception/utk.exception/include/utk/exception/RuntimeError/CMakeLists.txt
#
# Description: Utility Tool Kit Exception Library sources listing.


set (HEADERS
  FileError.hpp
  FormatError.hpp
  FileNotFound.hpp
  InvalidRuntimeArgument.hpp
  NotImplemented.hpp
  ObjectDoesNotExist.hpp
  TypeError.hpp
  ValueError.hpp
  )


file (RELATIVE_PATH
  PREFIX
  ${PROJECT_SOURCE_DIR}
  ${CMAKE_CURRENT_LIST_DIR}
  )


foreach (HEADER IN LISTS HEADERS)
  utk_cmake_target_sources (
    TARGET  ${${PROJECT_NAME}_TARGET_LIST}
    PRIVATE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/${HEADER}>
    $<INSTALL_INTERFACE:${PREFIX}/${HEADER}>
    )
endforeach (HEADER IN HEADERS)
