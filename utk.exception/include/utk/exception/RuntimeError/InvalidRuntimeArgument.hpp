// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name:
// utk.exception/utk.exception/include/utk/exception/RuntimeError/InvalidRuntimeArgument.hpp
//
// Description: The exception class for reporting an invalid argument value
//              passed to a function as a result of an external runtime error.


#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_INVALIDRUNTIMEARGUMENT_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_INVALIDRUNTIMEARGUMENT_HPP


#include "utk/exception/namespace.hpp"
#include "utk/exception/utk_exception_export.h"

#include "utk/exception/RuntimeError.hpp"


UTK_EXCEPTION_NAMESPACE_OPEN


/**
   @brief The exception class for reporting an invalid argument value passed to
   a function as a result of an external runtime error

   @details Exceptions of this type should be thrown whenever a function
   argument has an invalid value that most likely result from any kind of
   runtime error: wrong user input, network failure, etc. This kind of error may
   not be a reason of an error in software.
*/
struct InvalidRuntimeArgument : public RuntimeError {
	typedef boost::error_info< struct ArgumentName, std::string > ArgumentName;
	typedef boost::tuple< ArgumentName, Description > ArgumentInfo;
};


UTK_EXCEPTION_NAMESPACE_CLOSE


#endif // UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_INVALIDRUNTIMEARGUMENT_HPP
