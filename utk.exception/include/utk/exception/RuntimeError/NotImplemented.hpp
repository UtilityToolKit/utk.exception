// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name:
// utk.exception/utk.exception/include/utk/exception/RuntimeError/NotImplemented.hpp
//
// Description: The exception class for handling the situations where the
//              functionality is not implemented.


#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_NOTIMPLEMENTED_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_NOTIMPLEMENTED_HPP


#include "utk/exception/namespace.hpp"
#include "utk/exception/utk_exception_export.h"

#include "utk/exception/RuntimeError.hpp"


UTK_EXCEPTION_NAMESPACE_OPEN


/**
   @brief The exception class for handling the situations where the
   functionality is not implemented
*/
struct NotImplemented : virtual RuntimeError {};


UTK_EXCEPTION_NAMESPACE_CLOSE


#endif // UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_NOTIMPLEMENTED_HPP
