// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name:
// utk.exception/utk.exception/include/utk/exception/RuntimeError/ObjectDoesNotExist.hpp
//
// Description: The exception class for handling the situations where some
//              object that is expected to exist does not exist.


#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_OBJECTDOESNOTEXIST_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_OBJECTDOESNOTEXIST_HPP


#include "utk/exception/namespace.hpp"
#include "utk/exception/utk_exception_export.h"

#include "utk/exception/RuntimeError.hpp"


UTK_EXCEPTION_NAMESPACE_OPEN


/**
   @brief The exception class for handling the situations where some object that
   is expected to exist does not exist

   @details This error is not always the same as null pointer exception, because
   it also covers situations where an object in question is expected to exist
   outside of the program. Shared semaphor is an example of such an object.
*/
struct ObjectDoesNotExist : virtual RuntimeError {};


UTK_EXCEPTION_NAMESPACE_CLOSE


#endif // UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_RUNTIMEERROR_OBJECTDOESNOTEXIST_HPP
