#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_VARIANTEXCEPTION_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_VARIANTEXCEPTION_HPP


#include <string>
#include <type_traits> // is_base_of, is_constructible, is_same
#include <utility>     // move

#include <boost/exception/exception.hpp>

#include <boost/format.hpp>

#include <boost/variant.hpp>

#include "utk/exception/namespace.hpp"

#include "utk/exception/VariantExceptionWhat.hpp"


UTK_EXCEPTION_NAMESPACE_OPEN


template < class Base_, class... ExceptionVariants_ >
class VariantException : public Base_ {
public:
	static_assert (
	    ((::std::is_same< ::boost::exception, Base_ >::value) ||
	     ::std::is_base_of< ::boost::exception, Base_ >::value),
	    "Base class should be related to boost::exception");

	static_assert (
	    sizeof...(ExceptionVariants_),
	    "At least one exception variant is required");

	using Base = Base_;

	using VariantExceptionType =
	    VariantException< Base, ExceptionVariants_... >;

	using Error = ::boost::variant< ExceptionVariants_... >;


	template <
	    class Arg_,
	    class... Args_,
	    class = ::std::enable_if_t<
	        ::std::is_constructible< Error, Arg_, Args_... >::value > >
	VariantException (Arg_&& i_arg, Args_&&... i_args)
	    : error_{::std::forward< Arg_ > (i_arg),
	             ::std::forward< Args_ > (i_args)...} {
	}


	char const* what () const noexcept override {
		/*
		  Information may be added between calls so the message string is
		  generated each time anew.
		*/
		what_ = ::std::move (
		    (::boost::format ("{%1%}\n"
		                      "Nested exception:\n"
		                      "{%2%}") %
		     Base::what () %
		     ::boost::apply_visitor (detail::VariantExceptionWhat{}, error_))
		        .str ());

		return what_.c_str ();
	}


protected:
	Error error_;

	/*
	  Required for storing what() result. Simpler then boost::exception
	  mechanism.
	*/
	mutable ::std::string what_;
};


UTK_EXCEPTION_NAMESPACE_CLOSE


#undef FMT_STRING_ALIAS


#endif /* UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_VARIANTEXCEPTION_HPP */
