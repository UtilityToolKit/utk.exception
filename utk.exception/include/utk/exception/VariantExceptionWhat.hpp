#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_VARIANTEXCEPTIONWHAT_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_VARIANTEXCEPTIONWHAT_HPP


#include <exception>

#include <boost/variant/static_visitor.hpp>

#include "utk/exception/namespace.hpp"


UTK_EXCEPTION_NAMESPACE_OPEN


namespace detail {
	class VariantExceptionWhat : public ::boost::static_visitor< const char* > {
	public:
		template < class Error_, class = std::enable_if_t< !std::is_same_v< Error_, std::exception_ptr > > >
		const char* operator() (const Error_& i_error) const noexcept {
			return i_error.what ();
		}

		const char* operator() (const ::std::exception_ptr& i_error) const noexcept {
			try {
				::std::rethrow_exception (i_error);
			}
			catch (const ::std::exception& i_error) {
				return i_error.what ();
			}
			catch (...) {
				return "Non-standard exception";
			}
		}
	};
} // namespace detail


UTK_EXCEPTION_NAMESPACE_CLOSE


#endif /* UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_VARIANTEXCEPTIONWHAT_HPP */
