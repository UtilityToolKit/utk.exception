// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// File name:
// utk.exception/utk.exception/include/utk/exception/ExceptionBase.hpp
//
// Description: Macros for opening and closing the project namespace.


#ifndef UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_NAMESPACE_HPP
#define UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_NAMESPACE_HPP


#define UTK_EXCEPTION_NAMESPACE_OPEN                                           \
	namespace utk {                                                            \
		inline namespace exception {                                           \
			inline namespace v0_5_0 {


#define UTK_EXCEPTION_NAMESPACE_CLOSE                                          \
	}                                                                          \
	}                                                                          \
	}


#endif /* UTK_EXCEPTION_INCLUDE_UTK_EXCEPTION_NAMESPACE_HPP */
