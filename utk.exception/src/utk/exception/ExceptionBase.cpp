#include "utk/exception/ExceptionBase.hpp"

#include <boost/exception/diagnostic_information.hpp>


UTK_EXCEPTION_NAMESPACE_OPEN


const char* ExceptionBase::what () const throw () {
	return boost::diagnostic_information_what (*this);
}


UTK_EXCEPTION_NAMESPACE_CLOSE
